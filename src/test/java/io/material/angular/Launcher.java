package io.material.angular;

import io.material.angular.Models.ActionModel;
import io.material.angular.Models.Actions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Launcher {

    // путь до вэб-драйвера:
    private String pathToWebDriver = "C:/TEMP/chromedriver_win32/chromedriver.exe";

    // путь до конфигурационного JSON-файла:
    private String pathToConfig = System.getProperty("user.dir") + "/src/test/resources/config.json";

    // путь до директории, хранящей скриншоты
    private String pathToScreenshot = System.getProperty("user.dir") + "/src/test/resources/screenshots/screenshot_";

    // путь до директории, хранящей исходные коды страниц
    private String pathToPageSourceCode = System.getProperty("user.dir") + "/src/test/resources/pageSourceCode/pageSourceCode_";

    private WebDriver driver;

    private Actions actions;

    @Before
    public void beforeTesting() throws IOException {
        System.setProperty("webdriver.chrome.driver", pathToWebDriver);
        driver = new ChromeDriver();
        ObjectMapper mapper = new ObjectMapper();
        File config = new File(pathToConfig);
        // десериализвация объекта из JSON-файла в объект Actions:
        actions = mapper.readValue(config, Actions.class);
    }

    @Test
    public void actionsFromConfigJson() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        driver.manage().window().maximize();

        ActionModel nextAction;

        TaskTests taskTests = new TaskTests(
                driver,
                pathToScreenshot,
                pathToPageSourceCode
        );
        Class taskTestsClass = taskTests.getClass();
        Method method;

        for (int i = 0; i < actions.getActions().length; i++) {
            nextAction = actions.getActions()[i];
            try {
                // ищем метод с одним параметром типа String
                method = taskTestsClass.getDeclaredMethod(
                        nextAction.getAction(),
                        String.class
                );
                method.setAccessible(true);
                method.invoke(
                        taskTests,
                        nextAction.getParams()
                );
            } catch (java.lang.NoSuchMethodException e) {
                // ищем метод без параметров
                method = taskTestsClass.getDeclaredMethod(
                        nextAction.getAction()
                );
                method.setAccessible(true);
                method.invoke(
                        taskTests
                );
            }
        }
    }

    @After
    public void afterTesting() {
        driver.quit();
    }
}
