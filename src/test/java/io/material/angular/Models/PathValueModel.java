package io.material.angular.Models;

public class PathValueModel {

    String path;

    String value;

    public PathValueModel(String path, String value) {
        this.path = path;
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public String getValue() {
        return value;
    }
}
