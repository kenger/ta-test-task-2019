package io.material.angular.Models;

public class ActionModel {

    private String action;

    private String params;

    private String description;

    public String getAction() {
        return action;
    }

    public String getParams() {
        return params;
    }

    public String getDescription() {
        return description;
    }

}
