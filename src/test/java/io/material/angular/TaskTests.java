package io.material.angular;

import io.material.angular.Models.PathValueModel;

import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TaskTests {

    private WebDriver driver;
    private String pathToScreenshot;
    private String pathToPageSourceCode;

    public TaskTests(
            WebDriver driver,
            String pathToScreenshot,
            String pathToPageSourceCode) {
        this.driver = driver;
        this.pathToScreenshot = pathToScreenshot;
        this.pathToPageSourceCode = pathToPageSourceCode;
    }

    // переход по ссылке
    private void openUrl(String url) {
        driver.get(url);
    }

    // нажатие на элемент
    private void click(String params) {
        WebElement element = waitFor(params);
        element.click();
    }

    // вставка значения в элемент (поле)
    private void setValue(String params) {
        PathValueModel pathValueModel = getPathAndValue(params);

        WebElement element = waitFor(pathValueModel.getPath());
        element.sendKeys(pathValueModel.getValue());
    }

    // проверка на отображание элемента на вэб-странице
    private void checkVisibility(String params) {
        WebElement element = waitFor(params);
        element.isDisplayed();
    }

    // снимок экрана (скриншот)
    private void takeScreenshot() throws IOException {
        TakesScreenshot takesScreenshot = ((TakesScreenshot) driver);
        File screenshot = takesScreenshot.getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(screenshot, new File(pathToScreenshot + getDateAndTime() + ".png"));
    }

    // проверка на совпадение адреса страницы
    private void checkUrl(String expectedUrl) {
        assert driver.getCurrentUrl().equals(expectedUrl) : "Page's URL doesn't equal to " + expectedUrl;
    }

    // проверка на совпадение заданного заголовка страницы
    private void checkTitle(String expectedTitle) {
        assert driver.getTitle().equals(expectedTitle) : "Page's title doesn't equal to " + expectedTitle;
    }

    // запись исходного текста страницы в файл
    private void savePageSourceCode() throws IOException {
        String sourceCode = driver.getPageSource();
        FileUtils.writeStringToFile(new File(pathToPageSourceCode + getDateAndTime() + ".html"), sourceCode);
    }

    // переход на предыдущую страницу
    private void navigateBack() {
        driver.navigate().back();
    }

    // переход на следующую страницу (которую уже посещали)
    private void navigateForward() {
        driver.navigate().forward();
    }

    // перезагрузка страницы
    private void refreshPage() {
        driver.navigate().refresh();
    }

    // переход на указанную (в поле params) вкладку браузера
    private void switchBrowserTabTo(String index) {
        int tabIndex = Integer.parseInt(index);

        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabIndex));
    }

    /* вспомогательный метод для ожидания подгрузки
    нужного элемента на вэб-странице*/
    private WebElement waitFor(String path) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath(path))
        );

        return driver.findElement(By.xpath(path));
    }

    /* вспомогательный метод для генерации строки, содержащей время и дату
    в формате <дата>_<время>*/
    private String getDateAndTime() {
        Date date = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
        return formatter.format(date);
    }

    /* вспомогательный метод для конвертирования значения params
    из JSON-Файла в объект PathValueModel */
    private PathValueModel getPathAndValue(String params) {
        String path = params.substring(
                0,
                params.indexOf("|") - 1
        );
        String value = params.substring(
                params.indexOf("|") + 2
        );

        return new PathValueModel(path, value);
    }
}
